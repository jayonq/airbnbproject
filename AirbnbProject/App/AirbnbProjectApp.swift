//
//  AirbnbProjectApp.swift
//  AirbnbProject
//
//  Created by Айбар on 19.05.2024.
//

import SwiftUI
import FirebaseCore

class AppDelegate: NSObject, UIApplicationDelegate {
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    FirebaseApp.configure()

    return true
  }
}

@main
struct AirbnbProjectApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    
    let authManager = AuthManager(service: FirebaseAuthService())
    
    var body: some Scene {
        WindowGroup {
            ContentView(authManager: authManager)
        }
    }
}
