//
//  Regions.swift
//  AirbnbProject
//
//  Created by Айбар on 22.05.2024.
//

import CoreLocation

extension CLLocationCoordinate2D {
    static var losAngeles = CLLocationCoordinate2D(latitude: 34.0499998, longitude: -118.249999)
    static var miami = CLLocationCoordinate2D(latitude: 25.7602, longitude: -80.1959)
}
