//
//  PrimaryButtonModifier.swift
//  AirbnbProject
//
//  Created by Айбар on 23.05.2024.
//

import SwiftUI

struct PrimaryButtonModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.subheadline)
            .fontWeight(.semibold)
            .foregroundStyle(.white)
            .frame(width: 350, height: 40)
            .background(.pink)
            .clipShape(RoundedRectangle(cornerRadius: 8))
    }
}


