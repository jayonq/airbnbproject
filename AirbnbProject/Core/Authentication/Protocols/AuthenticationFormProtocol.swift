//
//  AuthenticationFormProtocol.swift
//  AirbnbProject
//
//  Created by Айбар on 23.05.2024.
//

import Foundation

protocol AuthenticationFormProtocol {
    var  formIsValid: Bool { get }
}
