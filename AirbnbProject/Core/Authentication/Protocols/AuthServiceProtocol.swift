//
//  AuthServiceProtocol.swift
//  AirbnbProject
//
//  Created by Айбар on 24.05.2024.
//

import Foundation

protocol AuthServiceProtocol{
    func login(withEmail email: String, password: String) async throws -> String?
    
    func createUser(withEmail email: String, password: String, fullName: String) async throws -> String?
//    func createUser(withEmail email: String, password: String, fullName: String) async throws -> String?
    
    func signout()
}
