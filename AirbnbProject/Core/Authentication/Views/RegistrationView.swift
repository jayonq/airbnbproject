//
//  RegistrationView.swift
//  AirbnbProject
//
//  Created by Айбар on 23.05.2024.
//

import SwiftUI

struct RegistrationView: View {
    @State private var email = ""
    @State private var password = ""
    @State private var fullName = ""
    @StateObject var viewModel = RegistrationViewModel(service: MockAuthService())
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        VStack {
            Spacer()
            
            Image(.airbnbAppIcon)
                .resizable()
                .scaledToFit()
                .frame(width: 120, height: 120)
                .padding()
            
            VStack {
                TextField("Enter youre email", text: $email)
                    .modifier(PrimaryTextFieldModifier())
                
                TextField("Enter youre full name", text: $fullName)
                    .modifier(PrimaryTextFieldModifier())
                
                SecureField("Enter youre password", text: $password)
                    .modifier(PrimaryTextFieldModifier())
            }
            
            Button{
                Task { await viewModel.createUser(
                    withEmail: email,
                    password: password,
                    fullName: fullName
                )}
            } label: {
                Text("Registration")
                    .modifier(PrimaryButtonModifier())
            }
            .padding(.vertical)
            .disabled(!formIsValid)
            .opacity(formIsValid ? 1.0 : 0.7)
            
            Spacer()
            
            Divider()
            
            Button{
                dismiss()
            } label: {
                HStack (spacing: 2) {
                    Text("Already have an account?")
                    Text("Sign In")
                        .fontWeight(.semibold)
                }
                .font(.footnote)
            }
            .padding(.vertical)
        }
    }
}

extension RegistrationView: AuthenticationFormProtocol {
    var formIsValid: Bool {
        return !email.isEmpty &&
        email.contains("@") &&
        !password.isEmpty &&
        password.count > 6 &&
        !fullName.isEmpty
    }
}

#Preview {
    RegistrationView()
}
