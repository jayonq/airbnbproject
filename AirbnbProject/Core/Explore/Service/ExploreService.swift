//
//  ExploreService.swift
//  AirbnbProject
//
//  Created by Айбар on 22.05.2024.
//

import Foundation

class ExploreService {
    
    func fetchingListings() async throws -> [Listing] {
        return DeveloperPreview.shared.listings
    }
}
